"use strict"
//Testing prototype-manufactured set and get functions
function SimpleDictionary() {
	this.keys = []
	this.values = []
}
SimpleDictionary.prototype.set = function(key, value) {
	this.keys.push(key)
	this.values.push(value)
}
SimpleDictionary.prototype.get = function(lookupKey) {
	for (var i = 0; i < this.keys.length; i++) {
		var key = this.keys[i];
		if (key === lookupKey) {
			return this.values[i]
		}
	}
}

// var Letters = new SimpleDictionary();
// Letters.set(1, 'a')
// Letters.set(2, 'b')
// console.log(Letters.get(2))



//Recreating a HashMap
function HashMap () {
	this.bucketCount = 10000;
	this.buckets = []
	for (var i = 0; i < this.bucketCount; i++) {
		this.buckets.push(new SimpleDictionary())
	}
}
HashMap.prototype.hashFunction = function(key) {
	var hash = 0;
	for (var i = 0; i < key.length; i++) {
		hash += key.charCodeAt(i)
	}
	return hash;
}
HashMap.prototype.getBucketIndex = function(key) {
	return this.hashFunction(key) % this.bucketCount
	//for each key, hash % bucketCount (10k)
}
HashMap.prototype.getBucket = function(key) {
	return this.buckets[this.getBucketIndex(key)]
	//gets the bucket at index ????????
}
HashMap.prototype.set = function(key, value) {
	this.getBucket(key).set(key, value)
}
HashMap.prototype.get = function(lookupKey) {
	return this.getBucket(lookupKey).get(lookupKey)
}

//makeid function
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

//Testing HashMap
var dict = new HashMap();

var keys = []
var values = []
for (var i = 0; i < 10000; i++) {
	keys.push(makeid())
	values.push(Math.round())
}

console.time("SET")
for (var i = 0; i < keys.length; i++) {
	dict.set(keys[i], values[i])
}
console.timeEnd("SET")

console.time("GET")
for (var i = 0; i < keys.length; i++) {
	var val = dict.get(keys[i])
}
console.timeEnd("GET")