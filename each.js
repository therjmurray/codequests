"use strict"

//_each

var myArray = [1, 2, 3, 4, 5]
var rjObject = {name: 'rj', age: 27}
var myObject = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}]


const iterator = x => {
	console.log(x)
}

const _each = (list, iteratee) => {
	var a = []

	for (let i = 0; i < list.length; i++) {
		iteratee(list[i]);
	}

	return a
}

_each(myArray, iterator)

myArray.forEach(x => {
	console.log(x)
})
