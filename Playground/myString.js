"use strict"

var addy = "1.1.1.1"

const defangeIPaddr = address => {
	var defanged = ""

	for (let i = 0; i < address.length; i++) {
		if (address.charAt(i) == '.') {
			defanged += '[.]'
		} else {
			defanged += address.charAt(i)
		}
	}

	return defanged
}

console.log(defangeIPaddr(addy))