"use strict"
//_pluck extracts a list of property values from object
var myObject = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}]

const _pluck = (list, propertyName) => {
	var a = []

	for (let i = 0; i < list.length; i++) {
		a.push(list[i].propertyName)
	}

	return a
}

console.log(_pluck(myObject, 'name'))