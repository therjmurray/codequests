"use strict"

var myArray = [1, 2, 3, 4, 5]

const pred = x => {
	return x > 2
}


const _filter = (list, predicate) => {
	var a = []

	for (let i = 0; i < list.length; i++) {
		if (predicate(list[i])){
			a.push(list[i])
		}
	}

	return a
}

console.log(_filter(myArray, pred))