"use strict"
//_invoke calls the method named methodName on each value in the list
var myArray = [[5, 1, 7], [3, 2, 1]]

const _invoke = (list, methodName) => {

	for (let i = 0; i < list.length; i++) {
		list[i][methodName]()
	}

}

/*let x = [3, 2, 1]
console.log(x.sort())*/

_invoke(myArray, 'sort')
console.log(myArray)