"use strict"
//_every returns true if all of the values in the list pass the predicate

var myArray = [1, 2, 3, 4, 5];
var myObject = {name: "joe", age: 25}

const pred = x => {
	return x >= 1
}

const _every = (list, predicate) => {
	var truthCount = 0;
	for (let i = 0; i < list.length; i++) {
		if (predicate(list[i])) {
			truthCount++
		} else {
			return false
		}
	}
	if (truthCount == list.length) {
		return true
	}
}

console.log(_every(myArray, pred))