"use strict"
//_some, returns true if any of the values in the list pass the pred
var myArray = [1, 2, 3, 4, 5];
var myObject = {name: "joe", age: 25}

const pred = x => {
	return x > 3
}

const _some = (list, predicate) => {
	var truthTest = false;
	for (let i = 0; i < list.length; i++) {
		if (predicate(list[i])) {
			truthTest = true;
			break;
		}
	}
	return truthTest
}

console.log(_some(myArray, pred))