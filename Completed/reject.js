"use strict"
var myArray = [1, 2, 3, 4, 5];
var myObject = {name: "joe", age: 25}

const pred = x => {
	return x > 3
}

const _reject = (list, predicate) => {
	var a = []
		for (let i = 0; i < list.length; i++) {
			if (predicate(list[i]) === false) {
				a.push(list[i])
			}
		}
	return a
}

console.log(_reject(myArray, pred))