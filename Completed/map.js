var arr = [1, 2, 3, 4, 5];

let timesFive = x => {
	return x * 5
}

let _map = (list, predicate) => {
	var a = []
	for (i = 0; i < list.length; i++){
		a.push(predicate(list[i]))
	}
	return a
}

console.log(_map(arr, timesFive))