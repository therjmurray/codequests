"use strict"
//_contains returns true if the value is present in the list
//uses indexOf internally if list is an Array
//Use fromIndex to start your search at a given index

var myArray = [1, 2, 3, 4, 5];
var myObject = {name: "joe", age: 25}


const _contains = (list, value) => {
	var truthTest = false;
	for (let i = 0; i < list.length; i++) {
		if (value == list[i]) {
			truthTest = true
		}
	}
	return truthTest
}

console.log(_contains(myArray, 3))