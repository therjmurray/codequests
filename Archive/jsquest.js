"use strict";
const handyArray = ['a', 'NaN', 'false'];

/***************
forEach Example
***************/
function logElementsOfArray(arr){

	arr.forEach(function(element) {
		console.log(element);
	});

};

var arr1 = [...handyArray];
//Uncomment line below to test
//logElementsOfArray(arr1);




/**********
Map Example
**********/
var arr2 = [1, 4, 9, 16];

function useMap(arr) {
	const map1 = arr.map(x => x * 2);
	console.log(map1);
}

//Uncomment line below to test
//useMap(arr2);




/*************
Reduce Example
*************/
var arr3 = [...arr2];

function useReduce(arr) {
	const reducer = (accumulator, currentValue) => accumulator + currentValue;	

	console.log(arr.reduce(reducer));
}

//Uncomment line below to test
//useReduce(arr2);




/******************
reduceRight Example
******************/
var arr4 = [[0, 1], [2, 3], [4, 5]];

function useReduceRight(arr) {
	const reducedArray = arr.reduceRight(
		(accumulator, currentValue) => accumulator.concat(currentValue)
	);

	console.log(reducedArray);
}

//Uncomment line below to test
//useReduceRight(arr4);




/*************
find() Example
*************/
var arr5 = [5, 3894, 842, 28, 104];

var isGreaterThanFive = (x) => {
	return x > 5
}

const useFind = (arr, predicate) => {
	var found = arr.find(function(element) {
		return element > 841;
	});

	console.log(found);
}

console.log(useFind(arr5, isGreaterThanFive))

//Uncomment line below to test
//useFind(arr5);




/****************
filter() Examples
****************/
var words1 = ['peanut', 'turkey', 'wizards', 'pony'];

const useFilter = (arr) => {
	const result = arr.filter(x => x.length > 6);
	console.log(result);
}

//Uncomment line below to test
//useFilter(words1);




/*********************
More filter() Examples
*********************/

var arr6 = [...arr5];

const isBigEnough = (value) => {
	return value >= 10;
}

let filtered = [12, 5, 8, 130, 34].filter(isBigEnough);

//Uncomment line below to test
//console.log(filtered);





/**************
_where Examples
**************/
var arr6 = [1, 5, 7, 18];

const learnWhere = (arr) => {
	var whereArray = arr.filter(function(v){return v >= 10});
	console.log(whereArray);
}

//Uncomment line below to test
//learnWhere(arr6);




/**********************
Promise.reject Examples
**********************/
//set promRej to true to test
let promRej = false;

if (promRej == true) {
	Promise.reject(new Error ('this program is fucked')).then(function() {
	}, function(error) {
		console.error(error);
	});
}




/**************
every() Example
**************/
var ds1 = [2, 30, 50, 20, 10];

const isAboveThreshold = (val) => {
	return val > 1;
}

//Uncomment line below to test
//console.log(ds1.every(isAboveThreshold));




/**************
some() Example
**************/
var ds2 = [...ds1];

const even = (element) => {
	return element % 2 === 0;
}

//Uncomment line below to test
//console.log(ds2.some(even));



/***********************************
_contains / includes() Example: Array
************************************/

const includesArray = () => {
	var ds3 = [1, 2, 3];

	console.log(ds3.includes(4));
}

//Uncomment line below to test
//includesArray();





/***********************************
_contains / includes() Example: String
************************************/

var sentence1 = 'Hello world.';

var trix = 'world.';


//console.log(`The word "${trix}" ${sentence1.includes(trix)? 'is' : 'is not'} in the sentence`);





/************************
_invoke / call() Examples
*************************/
function Product(name, price) {
	this.name = name;
	this.price = price;
}

function Food(name, price) {
	Product.call(this, name, price);
	this.category = 'food';
}

//console.log(new Food('cheese', 5).name);



/***************
_pluck Example
***************/
var stooges = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}];

const pluck = (arr, key) => {
	return arr.map(o => o[key]);
}

var pluckArray = pluck(stooges, 'name');
//
//console.log(pluckArray);




/***************
_max Example - returns maximum value in list
***************/
var maxArray = [...stooges];

const max = maxArray.reduce(function(prev, current) {
	return (prev.age > current.age) ? prev : current
});

//
//console.log(max);






/***************
_min Example - returns maximum value in list
***************/

var minArray = [...stooges];

const min = minArray.reduce(function(prev, current) {
	return (prev.age < current.age) ? prev : current
});

//console.log(min);





/***************
_sortBy Example
***************/
let numbers = [4, 2, 5, 1, 3];

numbers.sort((a, b) => b - a );

//console.log(numbers);


let objectList = [...stooges];

objectList.sort((a, b) => a - b);

//console.log(objectList);





/***************
_groupBy Example
***************/
var groupBy = function(xs, key) {

  return xs.reduce(function(rv, x) {

    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;

  }, {});

}

//console.log(groupBy(['one', 'two', 'three'], 'length'));
// => { '3': [ 'one', 'two' ], '5': [ 'three' ] }





/***************
_indexBy Example
***************/
function indexBy(list, iteratee, context) {

    return list.reduce((map, obj) => {
        const key = typeof iteratee === 'string' ? obj[iteratee] : iteratee.call(context, obj);
        map[key] = obj;
        return map;
    }, {});

}

//console.log(indexBy(stooges, 'age'));




/***************
_countBy Example
***************/
/*
var myList = [...stooges];

function countBy(collection, func) 
{
  var object = Object.create(null);

  collection.forEach(function(item) {

    var key = func(item);
    if (key in object) {
      ++object[key];
    } else {
      object[key] = 1;
    }
  });

  return object;
}
*/


/***************
_shuffle Example
***************/
var shuffleArray = [1, 2, 3, 4, 5, 6, 7];

function shuffle(a) {
	var j, x, i;
	for (i = a.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = a[i];
		a[i] = a[j];
		a[j] = x;
	}
	return a;
}

//console.log(shuffle(shuffleArray));




/***************
Single _sample Example
***************/
var sampleArray = [...shuffleArray];

function sample(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
}

//console.log(sample(sampleArray));



/***************
Multi _sample Example
***************/

/*
var sampleContainer = [];

function sample(arr, num) {
	for (let i = num; i < 0; i--){
		sampleContainer.push(arr[Math.floor(Math.random() * arr.length)]);
	}
	return sampleContainer;
}
*/




/***************
_toArray Example
***************/
var myInfo = "2267897894";

/*
function toArray(info) {
	for (let i = info.length; i > 0; i--) {
		return info.slice(0);
	}
}
*/

//console.log(Array.from(myInfo));





/************
_size of Array Example
************/

var sizeList = [...sampleArray]; //array

//console.log(sizeList.length);




/************
_size of Object Example
************/
var sizeList2 = [...stooges]; //object

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
}

//console.log(sizeList2);
//console.log(Object.size(sizeList2));





/************
_partition Example
************/
var partList = [...sampleArray];

const partition = (list, n) => {
	return list.length ? [list.splice(0, n)].concat(partition(list, n)) : [];
}

//console.log(partition(partList, 3));




/************
_compact Example
************/
var compactList = [0, 1, false, 2, '', null, undefined, NaN, 10, 11];

const compact = (list) => {
	var myFilterArray = list.filter(Boolean);
	return myFilterArray;
}

//console.log(compact(compactList));





/************
_first only Example
************/
var firstArray = [...sampleArray];

const first = (arr) => {
	return arr[0];
}

//console.log(firstArray);
//console.log(first(firstArray));




/************
_first n Example
************/
var nArray = [...sampleArray];

const firstN = (arr, n) => {
	var thisN = [];
	for (let i = 0; i < n; i++) {
		thisN.push(arr[i]);
	}
	return thisN;
}

//console.log(nArray);
//console.log(firstN(nArray, 4));




/************
_first n Example
************/
var bArray = [...sampleArray];

const firstb = (arr, b) => {
	return arr.splice(0, b);
}

//console.log(firstb(bArray, 3));


/************
_initial Example - return all but last, pass n to excluse last n elements
************/
var initialArray = [...sampleArray];

const initialN = (arr, n) => {
	var newArr = [];
	for (let i = 0; i < arr.length - n; i++) {
		newArr.push(arr[i]);
	}
	return newArr;
}

//console.log(initialArray);
//console.log(initialN(initialArray, 4));




/************
_last Example
************/
var lastArray = [...sampleArray];

const lastN = (arr, n) => {
	var lastArr = [];
	for (let i = arr.length - 1; i > arr.length - n - 1; i--) {
		lastArr.unshift(arr[i]);
	}
	return lastArr;
}

//console.log(lastN(lastArray, 4));




/************
_rest Example - Returns the rest of the elements in an array. Pass an index to return the values of the array from that index onward. 
************/

const restIndex = (arr, index) => {
	let meArray = [];
	for (let i = index; i < arr.length; i++) {
		meArray.push(arr[i]);
	}
	return meArray;
}

//console.log(restIndex(sampleArray, 5));





/************
_flatten Example
************/
let nestArray = [1, 2, [3, 4, [5, 6]]];

const flatten = (arr) => {
	const flat = [];
	
	arr.forEach(item => {
		if (Array.isArray(item)) {
			flat.push(...flatten(item));
		} else {
			flat.push(item);
		}
	});
	return flat;
}

//console.log(flatten(nestArray));




/************
_without Example
************/
let woArray = [...sampleArray];

const without = (arr, wo) => {
	let newArray = [];
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] !== wo) {
			newArray.push(arr[i]);
		}
	}
	return newArray;
}

//console.log(without(woArray, 3));




/************
_union Example 1
************/
var aExample = [1, 2, 3];
var bExample = [4, 5, 6];

const union = (a, b) => {
	return [...new Set([...a, ...b])];
}

//console.log(union(aExample, bExample));



/************
_union Example 2
************/
const union2 = (a, b) => {
	return a.concat(b);
}

//console.log(union(aExample, bExample));




/************
_intersection Example
************/
var yExample = [1, 2, 3, 4, 5];
var zExample = [4, 5, 6, 7, 8];

const intersection = (arr1, arr2) => {
	return arr1.filter(value => arr2.includes(value));
}

//console.log(intersection(yExample, zExample));




/************
_difference Example
************/
Array.prototype.diff = function(a) {
	return this.filter(function(i) {
		return a.indexOf(i) < 0;
	});
}

//console.log(yExample.diff(zExample));




/************
_uniq Example
************/
var abc = ['a', 'b', 'c', 'c', 'c']

const onlyUnique = (value, index, self) => {
	return self.indexOf(value) === index;
}

//console.log(abc.filter(onlyUnique));




/************
_zip Example
************/
var xa = [1, 2, 3];
var xb = ['a', 'b', 'c'];

const zip = (arr1, arr2) => {
	var xc = arr1.map(function(e, i) {
		return [e, arr2[i]];
	});
	return xc;
}


var zipped = zip(xa, xb);
console.log(zipped);




/************
_unzip Example
************/
//console.log(zipped);






/************
_object Example
************/

