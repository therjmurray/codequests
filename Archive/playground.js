"use strict"
var myObj = {name: "joe", age: 25}


const myFunc = () => {
	var a = Object.keys(myObj)
	a.forEach(e => {
		console.log(e)
	})

	console.log('-------')
	
	var b = Object.values(myObj)
	b.forEach(e => {
		console.log(e)
	})
	console.log('-------')
	var c= Object.entries(myObj)
	c.forEach(e => {
		console.log(e)
	})
}

myFunc()