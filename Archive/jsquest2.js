"use strict";
let myObject = [{name: 'Joe', age: 150}, {name: 'Jim', age: 120}, {name: 'George', age: 201}]

//_forEach
const _forEach = (list) => {
	list.forEach((e) => {
		console.log(e);
	})
}

//_forEach(myObject)

//_map
const _map = (list) => {
	return list.map((e, i) => {
		return [e, i];
	});
}

//console.log(_map(myObject));

//_reduce
const _reduce = (list) => {
	return list.reduce((acc, curr) => acc + curr)
}

//console.log(_reduce([1, 2 ,3, 4]));

//_reduceRight
const _reduceRight = (list) => {
	return list.reduce((a, b) => b.concat(a))
}

//console.log(_reduceRight([[1, 2], [3, 4], [5, 6]]));

//_find
const _find = (list) => {
	return list.find((e) => {
		return e > 5
	})
}

//console.log(_find([4, 5, 6, 7]))

//_filter
const _filter = (list) => {
	return list.filter(a => a.length > 6)
}

//console.log(_filter(['peanut', 'turkey', 'wizards', 'pony']))

//_findWhere
const _findWhere = (list, search) => {
	return list.filter(a =>	a == search)
}

//console.log(_findWhere([1, 2, 3], 3))

//_where****************************************************
const _where = (list, search) => {
	
}