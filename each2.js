"use strict"

//_each

var myArray = [1, 2, 3, 4, 5]
var myObject = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}]

const iterator = x => {
	console.log(x)
}

const _each = (list, iteratee) => {
	var a = []

	for (let i = 0; i < list.length; i++) {
		
		if (typeof list[i] === 'object' && !Array.isArray(list)) {
			console.log('this is an object')
		}

		if (Array.isArray(list)) {
			console.log('this is an array')
		}
	}
	return a
}
_each(myObject[1], iterator)