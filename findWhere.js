"use strict"
//looks through the list and returns the first value that matches all
//of the key-value pairs in properties
var myObj = {name: "joe", age: 25}
var myObj2 = [{name: "jim", age: 26}, {name: "joel", age: 27}]
	//var b = Object.keys(myObj)
	//var c = Object.values(myObj)
	//var d = Object.entries(myObj)
	// variable.hasOwnProperty() ==== truth test


///////////array.find is the key to solving this!
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find



const pred = x => {
	return x
}

const _findWhere = (list, predicate) => {
	var a;

	for (let i in list) {
		if (predicate(a)) {
			a = list[i]
		} else {
			return undefined
		}
	}
	return a;
}

console.log(_findWhere(myObj2, pred))